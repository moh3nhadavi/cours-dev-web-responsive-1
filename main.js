let mobileMenuBtn = document.querySelector(".mobile-menu-button");

mobileMenuBtn.addEventListener("click", () => {
    mobileMenuBtn.classList.toggle("active");
    /* On click on the menu button, toggle the visibility state of the menu */
    let secondaryMobileMenu = document.querySelector(".top-header .secondary-menu"),
        primaryMobileMenu = document.querySelector(".top-header .primary-menu");
    secondaryMobileMenu.classList.toggle("active");
    primaryMobileMenu.classList.toggle("active");
})